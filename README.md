Changelog:
  2.1.10
    * fixed select2 styles
    * moved RM+ icon for email layout to our server
    * fixed fast-edit links for select2
  2.1.9
    * select2 - 4.0.3
    * fixed modal windows
  2.1.8
    * fixed loading select2_extensions if select2 disabled
  2.1.7
    * moved helpers for postgreSQL
  2.1.6
    * add periodpicker
  2.1.5
    * dependency FontAwasome fot luxury_buttons
  2.1.0
    * refactored modal windows
  2.0.0
    * fixed auto-setting FontAwasome
  1.1.8
    * fixed auto-settings
  1.1.7
    * fixed favourite project sql to ansi sql
  1.1.6
    * added support for information about the license expiration
    * change Highcharts to jqPlot
  1.1.5:
    * added create_guid
    * modified modal_windows