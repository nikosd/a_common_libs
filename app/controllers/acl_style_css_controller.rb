class AclStyleCssController < ApplicationController
  def upload_icons
    if request.post?
      begin
        ACommonLibs::CssBtnIconsUtil.new(params[:css_icon])
        ACommonLibs::CssBtnIconsUtil.generate_css_file
      rescue Exception => ex
        render text: ex.message, status: 500
        return
      end

      render text: ACommonLibs::CssBtnIconsUtil.include_generated_css
      return
    end
    render layout: false
  end
end